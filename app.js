const dotenv = require('dotenv')
const logger = require("./utils/logger");
process.on('uncaughtException', err => {
    console.log('Uncaught Exception...')
    console.log(err)
    process.exit(1)
}) 
dotenv.config()
const mongoose=require('mongoose')
const app=require('./index')

const DB = process.env.DATABASE;
mongoose.connect(DB,{
    useNewUrlParser:true,
    useUnifiedTopology: true 
}).then(con=>{
    console.log("DB Connection successfull")
})

const PORT=process.env.PORT || 3000
const server=app.listen(PORT, () => {
    console.log(`Server running on PORT ${PORT}`)
})

// Error handling middleware
app.use((err, req, res, next) => {
  logger.error(err); 
  res.status(500).send("Internal Server Error"); // Send an error response to the client
});

process.on('unhandledRejection',err=>{
    console.log('Unhandled Rejection..')
    console.log(err.name,err.message)
    server.close(()=>{
        process.exit(1)
    })
})