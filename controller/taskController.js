const Task = require("../models/taskModel");
const Goal = require("../models/goalModel");
const UserLog = require("../models/userLogModel")
const AppError = require("../utils/appError");
const { MAX_NUMBER_TASK } = require("../constants");
const taskSchema = require("../schemas/taskSchema");
const validateSchema = require("../utils");

// Create a new task
exports.createTask = async (req, res) => {
  try {
    const validatedData = validateSchema(taskSchema, req.body);
    let goalId = req.params.id;
    const currentGoal = await Goal.findById(goalId);

    if (!currentGoal) {
      throw new AppError(`Invalid Goal Id.`);
    } 

    if (currentGoal.tasks.length >= MAX_NUMBER_TASK) {
      throw new AppError(`A goal can have maximum ${MAX_NUMBER_TASK} tasks.`);
    }

    const {
      taskName,
      quantity,
      frequency,
      reminder,
      reminderTime,
      logs,
    } = validatedData;

    const task = await Task.create({
      goalId,
      taskName,
      quantity,
      frequency,
      reminder,
      reminderTime,
      logs,
    });

    await Goal.findByIdAndUpdate(goalId, { $push: { tasks: task._id } });
    res.status(201).json({ message: "Task created successfully", task: task });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Get all tasks by goal ID
exports.getTasksByGoalId = async (req, res) => {
  try {
    // Fetch tasks along with their associated logs
    const taskId = req.params.id;
    const tasks = await Task.findById(taskId).populate("logs");
    res.status(200).json(tasks);
  } catch (error) {
    console.error("Error getting tasks with logs:", error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

exports.makeTaskAsComplete = async (req, res) => {
  try {
    const { taskId, goalId } = req.params;
    const userId = req.user._id;

    // Check if the task has already been completed the maximum number of times for the given frequency and date
    const task = await Task.findById(taskId);
    const today = new Date();
    const startOfToday = new Date(
      today.getFullYear(),
      today.getMonth(),
      today.getDate()
    );
    const endOfToday = new Date(
      today.getFullYear(),
      today.getMonth(),
      today.getDate() + 1
    );
    const logsCount = await UserLog.countDocuments({
      taskId,
      timestamp: { $gte: startOfToday, $lt: endOfToday },
    });
    if (logsCount >= task.frequency) {
      return res
        .status(400)
        .json({
          message:
            "Task cannot be completed more than the specified frequency for today.",
        });
    }

    // Create a new user log entry
    const userLog = new UserLog({
      userId,
      goalId,
      taskId,
      completed: true,
    });
    await userLog.save();

    // Update the task to reflect completion
    await Task.findByIdAndUpdate(taskId, { $push: { logs: userLog._id } });

    res
      .status(200)
      .json({ message: "Task marked as complete and logged successfully." });
  } catch (error) {
    console.error("Error marking task as complete:", error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

// Update task by ID
exports.updateTask = async (req, res) => {
  try {
    const validatedData = validateSchema(taskSchema, req.body);

    const {
      taskName,
      quantity,
      frequency,
      reminder,
      reminderTime,
      logs,
    } = validatedData;

    const updatedTask = await Task.findByIdAndUpdate(
      req.params.id,
      { taskName, quantity, frequency, reminder, reminderTime, logs },
      { new: true }
    );
    if (!updatedTask) {
      return res.status(404).json({ message: "Task not found" });
    }
    res
      .status(200)
      .json({ message: "Task updated successfully", task: updatedTask });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Delete task by ID
exports.deleteTask = async (req, res) => {
  try {
    const deletedTask = await Task.findById(req.params.id);
    if (!deletedTask) {
      return res.status(404).json({ message: "Task not found" });
    }

    await deletedTask.deleteOne();
    //If the child object is associated with a parent object, 
    //update the parent object to remove the reference to the child object
    if (deletedTask.goalId) {
      const goal = await Goal.findById(deletedTask.goalId);
      
      if (goal) {
        goal.tasks = goal.tasks.filter(
          (taskId) => taskId.toString() !== deletedTask._id.toString()
        );
        await goal.save();
      }
    }

    // await task.delete();

    res.status(200).json({ message: "Task deleted successfully" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
