const { createLogger, transports, format } = require("winston");

// Define the logger configuration
const logger = createLogger({
  level: "error", 
  format: format.combine(
    format.timestamp(), 
    format.json() 
  ),
  transports: [
    new transports.File({ filename: "error.log" }), // Log errors to error.log file
  ],
});


logger.add(
    new transports.Console({
      format: format.combine(
        format.colorize(), 
        format.simple()
      ),
    })
);


module.exports = logger;
